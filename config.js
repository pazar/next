const environment = {
  development: {
    graphQlEndPoint: 'http://localhost:3030/api/graphql',
  },
  production: {
    graphQlEndPoint: 'https://pazar-graphql.herokuapp.com/api/graphql',
  },
}[process.env.NODE_ENV || 'development'];
module.exports = Object.assign({
}, environment);
