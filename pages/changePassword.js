import React from 'react';
import page from '../hocs/page';
import ChangePasswordForm from '../components/ChangePasswordForm/ChangePasswordForm';

const changePassword = () =>
  (<div className="container">
    <ChangePasswordForm />
  </div>);
export default page(changePassword);
