import gql from 'graphql-tag';
import page from '../hocs/page';
import LoginForm from '../components/LoginForm/LoginForm';
import graphql from '../util/graphql';

const index = () =>
  <div className="container">
    <LoginForm />
  </div>;

const query = gql`
  query{
    user{
      email
    }
  }
`;

export default page(graphql(query)(index));
