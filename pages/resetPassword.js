import React, { PropTypes } from 'react';
import page from '../hocs/page';
import ResetPasswordForm from '../components/ResetPasswordForm/ResetPasswordForm';

const resetPassword = props => (<div className="container">
  <ResetPasswordForm token={props.query.token} />
</div>);

resetPassword.propTypes = {
  query: PropTypes.string,
  token: PropTypes.string,
};
export default page(resetPassword);
