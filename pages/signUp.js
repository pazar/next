import React from 'react';
import page from '../hocs/page';
import SignUpForm from '../components/SignUpForm/SignUpForm';

const signUp = () =>
  (<div className="container">
    <SignUpForm />
  </div>);
export default page(signUp);
