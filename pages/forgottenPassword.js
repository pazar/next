import React from 'react';
import page from '../hocs/page';
import ForgottenPasswordForm from '../components/ForgottenPasswordForm/ForgottenPasswordForm';

const forgottenPassword = () =>
  (<div className="container">
    <ForgottenPasswordForm />
  </div>);
export default page(forgottenPassword);
