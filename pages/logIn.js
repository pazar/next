import React from 'react';
import page from '../hocs/page';
import LoginForm from '../components/LoginForm/LoginForm';

const logIn = () =>
  (<div className="container">
    <LoginForm />
  </div>);
export default page(logIn);
