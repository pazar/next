import React, { Component, PropTypes } from 'react';
import { Form, Errors, Field, actions } from 'react-redux-form';
import { withApollo } from 'react-apollo';
import { connect } from 'react-redux';
import gql from 'graphql-tag';
import FormStyle from '../FormStyle/style.js';
import graphql from '../../util/graphql';

const forgottenPasswordMutation = gql`
  mutation forgottenPassword($email: String!){
  forgottenPassword(email: $email) {
    errors
  }
}
`;

@connect(null, { ...actions })
@graphql(forgottenPasswordMutation, { name: 'forgottenPassword' }, { ssr: false })
@withApollo

export default class ForgottenPasswordForm extends Component {
  static propTypes = {
    error: PropTypes.string,
    client: PropTypes.object,
    setErrors: PropTypes.func,
    forgottenPassword: PropTypes.func,
    setSubmitted: PropTypes.func,
    setSubmitFailed: PropTypes.func,
    setPending: PropTypes.func,
  };

  handleSubmit(user) {
    this.props.setPending('forms.forgottenPassword', true);
    this.props.forgottenPassword({
      variables: {
        ...user,
      },
    }).then((res) => {
      this.props.setPending('forms.forgottenPassword', false);
      if (!res.data.forgottenPassword.errors) {
        this.props.setSubmitted('forms.forgottenPassword');
        this.props.client.resetStore();
      } else {
        this.props.setSubmitFailed('forms.forgottenPassword');
        this.props.setErrors('forms.forgottenPassword', { '': { serverError: res.data.forgottenPassword.errors } });
      }
    });
  }

  render() {
    return (
      <div className="row">
        <FormStyle />
        <div className="col-md-6 col-md-offset-3" >
          <Form model="forms.forgottenPassword" onSubmit={user => this.handleSubmit(user)}>
            <h4 className="head-form">Forgotten Password</h4>
            <Errors className="list-unstyled text-danger" wrapper="ul" component="li" show={{ touched: true }} model="." messages={{ '': (val, message) => `${message}` }} />
            <div className="form-group">
              <Field model=".email" validators={{ required: val => val && val.length }}>
                <label className="label control-label" htmlFor="email">Email:</label>
                <input type="text" className="form-control" />
                <Errors className="list-unstyled text-danger" wrapper="ul" component="li" show={{ touched: true }} model=".email" messages={{ required: 'Invalid email.' }} />
              </Field>
            </div>
            <div className="form-group">
              <div>
                <button type="submit" className="btn btn-block register">
                  Forgotten Password
                </button>
              </div>
            </div>
          </Form>
        </div>
      </div>
    );
  }
}
