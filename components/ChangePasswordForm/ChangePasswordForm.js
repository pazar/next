import React, { Component, PropTypes } from 'react';
import { Form, Errors, Field, actions } from 'react-redux-form';
import { withApollo } from 'react-apollo';
import { connect } from 'react-redux';
import gql from 'graphql-tag';
import Router from 'next/router';
import graphql from '../../util/graphql';
import FormStyle from '../FormStyle/style.js';

const changePasswordMutation = gql`
  mutation changePassword($password: String!, $newPassword: String!){
  changePassword(password: $password, newPassword: $newPassword) {
    errors
  }
}
`;

@connect(null, { ...actions })
@graphql(changePasswordMutation, { name: 'changePassword' }, { ssr: false })
@withApollo

export default class ChangePasswordForm extends Component {
  static propTypes = {
    error: PropTypes.string,
    client: PropTypes.object,
    setErrors: PropTypes.func,
    changePassword: PropTypes.func,
    setSubmitted: PropTypes.func,
    setSubmitFailed: PropTypes.func,
    setPending: PropTypes.func,
  };

  handleSubmit(user) {
    this.props.setPending('forms.changePassword', true);
    this.props.changePassword({
      variables: {
        ...user,
      },
    }).then((res) => {
      this.props.setPending('forms.changePassword', false);
      if (!res.data.changePassword.errors) {
        this.props.setSubmitted('forms.changePassword');
        this.props.client.resetStore();
        Router.push('/');
      } else {
        this.props.setSubmitFailed('forms.changePassword');
        this.props.setErrors('forms.changePassword', { '': { serverError: res.data.changePassword.errors } });
      }
    });
  }

  render() {
    return (
      <div className="row">
        <FormStyle />
        <div className="col-md-6 col-md-offset-3" >
          <Form model="forms.changePassword" onSubmit={user => this.handleSubmit(user)}>
            <h4 className="head-form">Change Password</h4>
            <Errors className="list-unstyled text-danger" wrapper="ul" component="li" show={{ touched: true }} model="." messages={{ '': (val, message) => `${message}` }} />
            <div className="form-group">
              <Field model=".password" validators={{ required: val => val && val.length }}>
                <label className="label control-label" htmlFor="password">Password:</label>
                <input type="password" className="form-control" />
                <Errors className="list-unstyled text-danger" wrapper="ul" component="li" show={{ touched: true }} model=".password" messages={{ required: 'Please provide a password.' }} />
              </Field>
            </div>
            <div className="form-group">
              <Field model=".newPassword" validators={{ required: val => val && val.length }}>
                <label className="label control-label" htmlFor="newPassword">Password:</label>
                <input type="password" className="form-control" />
                <Errors className="list-unstyled text-danger" wrapper="ul" component="li" show={{ touched: true }} model=".newPassword" messages={{ required: 'Please provide a password.' }} />
              </Field>
            </div>
            <div className="form-group">
              <Field model=".confirmNewPassword" validators={{ required: val => val && val.length }}>
                <label className="label control-label" htmlFor="confirmNewPassword">Password:</label>
                <input type="password" className="form-control" />
                <Errors className="list-unstyled text-danger" wrapper="ul" component="li" show={{ touched: true }} model=".confirmNewPassword" messages={{ required: 'Please provide a password.' }} />
              </Field>
            </div>
            <div className="form-group">
              <div>
                <button type="submit" className="btn btn-block register">
                  Change Password
                </button>
              </div>
            </div>
          </Form>
        </div>
      </div>
    );
  }
}
