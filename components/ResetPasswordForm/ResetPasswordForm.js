import React, { Component, PropTypes } from 'react';
import { Form, Errors, Field, actions } from 'react-redux-form';
import { withApollo } from 'react-apollo';
import { connect } from 'react-redux';
import Router from 'next/router';
import gql from 'graphql-tag';
import graphql from '../../util/graphql';
import FormStyle from '../FormStyle/style.js';

const resetPasswordMutation = gql`
  mutation resetPassword($newPassword: String!, $token: String!){
  resetPassword(newPassword: $newPassword, token: $token) {
    errors
  }
}
`;

@connect(null, { ...actions })
@graphql(resetPasswordMutation, { name: 'resetPassword' }, { ssr: false })
@withApollo

export default class ResetPasswordForm extends Component {
  static propTypes = {
    error: PropTypes.string,
    token: PropTypes.string,
    client: PropTypes.object,
    setErrors: PropTypes.func,
    resetPassword: PropTypes.func,
    setSubmitted: PropTypes.func,
    setSubmitFailed: PropTypes.func,
    setPending: PropTypes.func,
  };

  handleSubmit(user) {
    this.props.setPending('forms.resetPassword', true);
    this.props.resetPassword({
      variables: {
        newPassword: user.newPassword,
        token: this.props.token,
      },
    }).then((res) => {
      this.props.setPending('forms.resetPassword', false);
      if (!res.data.resetPassword.errors) {
        this.props.setSubmitted('forms.resetPassword');
        this.props.client.resetStore();
        Router.push('/');
      } else {
        this.props.setSubmitFailed('forms.resetPassword');
        this.props.setErrors('forms.resetPassword', { '': { serverError: res.data.resetPassword.errors } });
      }
    });
  }

  render() {
    return (
      <div className="row">
        <FormStyle />
        <div className="col-md-6 col-md-offset-3">
          <Form model="forms.resetPassword" onSubmit={user => this.handleSubmit(user)} validators={{ '': { passwordsMatch: values => values.newPassword === values.confirmNewPassword } }}>
            <h4 className="head-form">Reset Password</h4>
            <Errors className="list-unstyled text-danger" wrapper="ul" component="li" show={{ touched: true }} model="." messages={{ '': (val, message) => `${message}` }} />
            <div className="form-group">
              <Field model=".newPassword" validators={{ required: val => val && val.length }}>
                <label className="label control-label" htmlFor="newPassword">New Password: </label>
                <input className="form-control" type="password" />
                <Errors className="list-unstyled text-danger" wrapper="ul" component="li" show={{ touched: true }} model=".newPassword" messages={{ required: 'Please provide a new Password.' }} />
              </Field>
            </div>
            <div className="form-group">
              <Field model=".confirmNewPassword" validators={{ required: val => val && val.length }}>
                <label className="label control-label" htmlFor="confirmNewPassword"> Confirm New Password: </label>
                <input className="form-control" type="password" />
                <Errors className="list-unstyled text-danger" wrapper="ul" component="li" show={{ touched: true }} model=".confirmNewPassword" messages={{ required: 'Please provide a confirm password.' }} />
                <Errors className="list-unstyled text-danger" wrapper="ul" component="li" show={{ touched: true }} model="." messages={{ passwordsMatch: 'Password, Confirm password is not matched' }} />
              </Field>
            </div>
            <div className="form-group">
              <div>
                <button type="submit" className="btn btn-block register">
                  Reset Password
                </button>
              </div>
            </div>
          </Form>
        </div>
      </div>
    );
  }
}
