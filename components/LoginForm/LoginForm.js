import React, { Component, PropTypes } from 'react';
import { Form, Errors, Field, actions } from 'react-redux-form';
import { withApollo } from 'react-apollo';
import { connect } from 'react-redux';
import gql from 'graphql-tag';
import cookie from 'react-cookie';
import Router from 'next/router';
import isEmail from 'validator/lib/isEmail';
import FormStyle from '../FormStyle/style.js';
import graphql from '../../util/graphql';

const loginMutation = gql`
  mutation loginUser($email: String!, $password: String!){
  login(email: $email, password: $password) {
    errors
    token
  }
}
`;

@connect(null, { ...actions })
@graphql(loginMutation, { name: 'loginUser' })
@withApollo

export default class LoginForm extends Component {
  static propTypes = {
    error: PropTypes.string,
    client: PropTypes.object,
    setErrors: PropTypes.func,
    loginUser: PropTypes.func,
    setSubmitted: PropTypes.func,
    setSubmitFailed: PropTypes.func,
    setPending: PropTypes.func,
  };

  handleSubmit(user) {
    this.props.setPending('forms.login', true);
    this.props.loginUser({
      variables: {
        ...user,
      },
    }).then((res) => {
      this.props.setPending('forms.login', false);
      if (!res.data.login.errors) {
        cookie.save('token', res.data.login.token, { path: '/' });
        this.props.setSubmitted('forms.login');
        this.props.client.resetStore();
        Router.push('/');
      } else {
        this.props.setSubmitFailed('forms.login');
        this.props.setErrors('forms.login', { '': res.data.login.errors });
      }
    });
  }

  render() {
    return (
      <div className="row">
        <FormStyle />
        <div className="col-md-6 col-md-offset-3" >
          <Form model="forms.login" onSubmit={user => this.handleSubmit(user)}>
            <h4 className="head-form">SIGN INTO YOUR ACCOUNT</h4>
            <Errors
              className="list-unstyled text-danger"
              wrapper="ul"
              component="li"
              show={{ touched: true }}
              model="."
              messages={{ '': (val, message) => `${message}` }}
            />
            <div className="form-group">
              <Field
                model=".email"
                validators={{
                  required: val => val && val.length,
                  email: val => isEmail(val),
                }}
              >
                <label className="label control-label" htmlFor="email">Email:</label>
                <input type="text" className="form-control" />
                <Errors className="list-unstyled text-danger" wrapper="ul" component="li" show={{ touched: true }} model=".email" messages={{ required: 'Please provide a email.', email: 'Invalid email' }} />
              </Field>
            </div>
            <div className="form-group">
              <Field model=".password" validators={{ required: val => val && val.length }}>
                <label className="label control-label" htmlFor="password">Password:</label>
                <input type="password" className="form-control" />
                <Errors className="list-unstyled text-danger" wrapper="ul" component="li" show={{ touched: true }} model=".password" messages={{ required: 'Please provide a password.' }} />
              </Field>
            </div>
            <div className="form-group">
              <div>
                <button type="submit" className="btn btn-block register">
                  Login
                </button>
              </div>
            </div>
          </Form>
        </div>
      </div>
    );
  }
}
