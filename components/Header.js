import UserNav from './UserNav';

const Header = () => (
  <div className="navbar navbar-default">
    <ul className="nav navbar-nav">
      <li><a href="/">Relate</a></li>
      <li><a href="/discover">Discover</a></li>
      <li><a href="/about">About</a></li>
    </ul>
    <UserNav />
  </div>
);

export default Header;
