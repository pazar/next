import React, { Component, PropTypes } from 'react';
import { Form, Errors, Field, actions } from 'react-redux-form';
import { connect } from 'react-redux';
import gql from 'graphql-tag';
import Router from 'next/router';
import isEmail from 'validator/lib/isEmail';
import graphql from '../../util/graphql';
import FormStyle from '../FormStyle/style.js';

const signUpMutation = gql`
  mutation signUpUser($email: String!, $password: String!){
  signup(email: $email, password: $password) {
  user{
  email
  password}
  errors
  token
  }
}
`;

@connect(null, { ...actions })
@graphql(signUpMutation, { name: 'signUpUser' }, { ssr: false })
export default class SignupForm extends Component {
  static propTypes = {
    error: PropTypes.string,
    setErrors: PropTypes.func,
    signUpUser: PropTypes.func,
    setSubmitted: PropTypes.func,
    setSubmitFailed: PropTypes.func,
    setPending: PropTypes.func,
  };

  handleSubmit(user) {
    this.props.setPending('forms.signUp', true);
    this.props.signUpUser({
      variables: {
        ...user,
      },
    }).then((res) => {
      this.props.setPending('forms.signUp', false);
      if (!res.data.signup.errors) {
        this.props.setSubmitted('forms.signUp');
        Router.push('/login');
      } else {
        this.props.setSubmitFailed('forms.signUp');
        this.props.setErrors('forms.signUp', { '': { serverError: res.data.login.errors } });
      }
    });
  }

  render() {
    return (
      <div className="row">
        <FormStyle />
        <div className="col-md-6 col-md-offset-3">
          <Form
            model="forms.signUp"
            onSubmit={user => this.handleSubmit(user)}
            validators={{
              '': {
                passwordsMatch: values => values.password === values.confirmPassword,
              },
            }}
          >
            <h4 className="head-form">CREATE AN ACCOUNT</h4>
            <Errors
              className="list-unstyled text-danger"
              wrapper="ul"
              component="li"
              show={{ touched: true }}
              model="."
              messages={{
                serverError: (val, message) => `${message}`,
              }}
            />
            <div className="form-group">
              <Field
                model=".email"
                validators={{
                  required: val => val && val.length,
                  email: val => isEmail(val),
                }}
              >
                <label className="label control-label" htmlFor="email"> Email: </label>
                <input type="text" className="form-control" />
                <Errors className="list-unstyled text-danger" wrapper="ul" component="li" show={{ touched: true }} model=".email" messages={{ required: 'Please provide a email.', email: 'Invalid email.' }} />
              </Field>
            </div>
            <div className="form-group">
              <Field model=".password" validators={{ required: val => val && val.length }}>
                <label className="label control-label" htmlFor="password"> Password: </label>
                <input className="form-control" type="password" />
                <Errors className="list-unstyled text-danger" wrapper="ul" component="li" show={{ touched: true }} model=".password" messages={{ required: 'Please provide a password.' }} />
              </Field>
            </div>
            <div className="form-group">
              <Field model=".confirmPassword" validators={{ required: val => val && val.length }}>
                <label className="label control-label" htmlFor="confirmPassword"> Confirm Password: </label>
                <input className="form-control" type="password" />
                <Errors className="list-unstyled text-danger" wrapper="ul" component="li" show={{ touched: true }} model=".confirmPassword" messages={{ required: 'Please provide a confirm password.' }} />
                <Errors className="list-unstyled text-danger" wrapper="ul" component="li" show={{ touched: true }} model="." messages={{ passwordsMatch: 'Password, Confirm password is not matched' }} />
              </Field>
            </div>
            <div className="form-group">
              <div>
                <button type="submit" className="btn btn-block register">
                  Sign up
                </button>
              </div>
            </div>
          </Form>
        </div>
      </div>
    );
  }
}
