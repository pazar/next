const FormStyle = () => (
  <div>
    <style jsx global>{`
  .register:hover, .register:active, .register:focus {
  border: none !important;
  outline: none !important;
  color: #fff;
}

.register {
  margin-top: 10px;
  margin-bottom: 20px;
  background-color: #00b8a4;
  border: 0;
  padding: 5px 10px;
  text-align: center;
  color: #fff;
  border-radius: 16px;
  font-size: 15px;
  line-height: 1.5;
  overflow: visible;
  font-weight: 700;
}
.head-form {
  font-size: 25px;
  color: #f54d87;
  font-weight: bold;
  margin-bottom: 20px;
}
.label {
  color: #6d6d6d;
  font-family: sans-serif;
  font-weight: 300;
  font-size: 15px;
  margin-left: -8px;
}
     `}</style>
  </div>
);
export default FormStyle;
