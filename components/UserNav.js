import { PropTypes, Component } from 'react';
import gql from 'graphql-tag';
import { withApollo } from 'react-apollo';
import cookie from 'react-cookie';
import Link from 'next/link';
import graphql from '../util/graphql';
/**
 * @return null if no user or the view to render
 */

const query = gql`
  query{
    user{
      email
    }
  }
`;

@graphql(query)
@withApollo
export default class ViewerHeader extends Component {
  static propTypes = {
    client: PropTypes.object,
    data: PropTypes.object,
  }

  state = {}

  render() {
    const { data: { user } } = this.props;
    if (!user) {
      return (<ul className="nav navbar-nav pull-right">
        <li>
          <Link href="/forgottenPassword"><a>Forgotten Password</a></Link>
        </li>
        <li>
          <Link href="/signUp"><a>Sign up</a></Link>
        </li>
        <li>
          <Link href="/logIn"><a>Login</a></Link>
        </li>
      </ul>);
    }
    return (
      <ul className="nav navbar-nav pull-right">
        <li>
          <a>
            {user.email}
          </a>
        </li>
        <li>
          <Link href="/changePassword"><a>Change Password</a></Link>
        </li>
        <li><a
          href="javascript:void(0)"
          className="btn btn-link"
          onClick={() => {
            cookie.remove('token', { path: '/' });
            this.props.client.resetStore();
          }}
        >
          Logout
        </a>
        </li>
      </ul>
    );
  }
}
