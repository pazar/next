import HtmlHead from '../components/HtmlHead';
import Header from '../components/Header';
import withData from './withData';

const page = (WrappedComponent) => {
  const Page = props => (
    <div>
      <HtmlHead />
      <Header />
      <WrappedComponent {...props} />
    </div>
  );
  return Page;
};

export default WrappedComponent => withData(page(WrappedComponent));
