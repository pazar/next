import { combineReducers } from 'redux';
import { combineForms } from 'react-redux-form';

const initialLoginState = {
  email: '',
  password: '',
};
const initialSignUpState = {
  email: '',
  password: '',
  confirmPassword: '',
};
const initialChangePasswordState = {
  password: '',
  newPassword: '',
  confirmNewPassword: '',
};
const initialForgottenPasswordState = {
  email: '',
};
const initialResetPasswordState = {
  newPassword: '',
  confirmNewPassword: '',
};

export default function getReducer(client) {
  return combineReducers({
    apollo: client.reducer(),
    forms: combineForms({
      login: initialLoginState,
      changePassword: initialChangePasswordState,
      signUp: initialSignUpState,
      forgottenPassword: initialForgottenPasswordState,
      resetPassword: initialResetPasswordState,
    }, 'forms'),
  });
}
