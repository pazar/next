const reactCookie = require('react-cookie');
const express = require('express');
const next = require('next');

const app = next({ dir: process.cwd(), dev: true });
const handle = app.getRequestHandler();

app.prepare()
  .then(() => {
    const server = express();
    server.use((req, res, done) => {
      reactCookie.plugToRequest(req, res);
      done();
    });
    server.get('*', (req, res) => handle(req, res));
    const PORT = process.env.PORT || 3000;
    server.listen(PORT, (err) => {
      if (err) throw err;
      console.log(`> Ready on http://localhost:${PORT}`);
    });
  });
